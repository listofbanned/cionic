import './App.css'
import { useState } from 'react'

// https://github.com/ctimmerm/axios-mock-adapter#readme
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
var mock = new MockAdapter(axios)

function App() {
  const [color, setColor] = useState('graphite')
  const [leg, setLeg] = useState('left')

  const [legSize, setLegSize] = useState({
    upper: '',
    lower: ''
  })
  const [min, max] = [0, 50]
  const [message, setMessage] = useState('')

  const [reqRarams, setReqParams] = useState({
    color: '',
    leg: '',
    size: legSize
  })

  // https://bobbyhadz.com/blog/react-number-input-min-max
  const handleInput = event => {
    if (event.target.value !== '') {
      const value = Math.max(min, Math.min(max, Number(event.target.value)))
      setLegSize({...legSize, [event.target.name]: value})
    } else {
      setLegSize({...legSize, [event.target.name]: event.target.value})
    }
  }

  const submit = () => {
    setReqParams({
      color: color,
      leg: leg,
      size: legSize
    })
    mock.onPost('/users').reply(function (config) {
      if (
        legSize.upper === '' ||
        legSize.lower === ''
      ) {
        return 500
      } else {
        return [200, {}]
      }
    })
    setReqParams({
      color: '',
      leg: '',
      size: ''
    })

    axios.post('/users', { params: { reqRarams }})
    .then(function (response) {
      setMessage(`{status: 200, message: "Success", params: {color: ${color}, leg: ${leg}, legSize: { upper: ${legSize.upper}, lower: ${legSize.lower}}}}`)
      setTimeout(() => {
        setMessage('')
      }, 1500)
    })
    .catch(() => {
      setMessage('{status: 500, message: "Error"}')
      setTimeout(() => {
        setMessage('')
      }, 1500)
    }
    )
    mock.resetHistory()
  }

  return (
    <div className='App'>
      <div className='App-container'>
         <div className='items'>
          <div className='card card-border'>
            <div className='card-title'>1. Select your color:</div>
              <div className='card-container'>
                <div
                  onClick={() => setColor('graphite')}
                  className={
                    color === 'graphite'
                      ? 'card-button selected'
                      : 'card-button'
                  }
                >
                  <div
                    className={
                      color === 'graphite'
                        ? 'circle graphite circle-selected'
                        : 'circle graphite'
                    }
                  ></div>
                  <span className='circle-text'>Graphite</span>
                </div>
                <div
                  onClick={() => setColor('gray')}
                  className={
                    color === 'gray'
                      ? 'card-button selected'
                      : 'card-button'
                  }
                >
                  <div
                    className={
                      color === 'gray'
                        ? 'circle gray circle-selected'
                        : 'circle gray'
                    }
                  ></div>
                  <span className='circle-text'>Gray</span>
                </div>
              </div>
          </div>

          <section>
            <div className='section-card middle card-border'>
              <div className='card-title'>2. Select which leg</div>
                <div className='card-container'>
                  <div
                    onClick={() => setLeg('left')}
                    className={
                      leg === 'left'
                        ? 'card-button selected'
                        : 'card-button'
                    }
                  >
                    <div
                      className={
                        leg === 'left'
                          ? 'circle circle-selected'
                          : 'circle'
                      }
                    >
                      <span className='circle-span circle-span-selected'>L</span>
                    </div>
                    <span className='circle-text'>Left</span>
                  </div>
                  <div
                    onClick={() => setLeg('right')}
                    className={
                      leg === 'right'
                        ? 'card-button selected'
                        : 'card-button'
                    }
                  >
                    <div
                      className={
                        leg === 'right'
                          ? 'circle circle-selected'
                          : 'circle'
                      }
                    >
                      <span className='circle-span'>R</span>
                    </div>
                    <span className='circle-text'>Right</span>
                  </div>
                </div>
            </div>

            <div className='section-card forms'>
              <div className='card-title'>3. Input your size:</div>
                <form className='form'>
                  <label className='form-label'>Upper Leg (inches)</label>
                  <input
                    type='text'
                    className='form-input'
                    placeholder='inches'
                    name='upper'
                    onChange={handleInput}
                  />
                  <label className='form-label'>Lower Leg (inches)</label>
                  <input
                    type='text'
                    className='form-input'
                    placeholder='inches'
                    name='lower'
                    onChange={handleInput}
                  />
                </form>
            </div>
          </section>
        </div>

        <div className='weird-line'></div>

        <div className='add-to-cart'>
        <button
          className='add-to-cart-btn'
          onClick={submit}
        >
            <span>Add to Cart</span>
          </button>
          <div className='weird-div'>
            <div className='weird-div-line'></div>
            <div className='weird-div-line'></div>
          </div>
        </div>
      </div>
      <span className='message'>{message}</span>
    </div>
  )
}

export default App;
